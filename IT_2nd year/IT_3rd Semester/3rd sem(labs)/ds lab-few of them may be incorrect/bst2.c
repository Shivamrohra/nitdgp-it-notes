#include<stdio.h>
#include<stdlib.h>
struct bst
{
int data;
struct bst *left;
struct bst *right;
};
struct bst *root;


void create()
{
root=NULL;
}


struct bst* search(int n)
{
struct bst *x=root;
while(x->left!=NULL&&x->right!=NULL)
{
  if(x->data>n)
  x=x->left;
  else if(x->data<n)
	x=x->right;
	else
	break;
}
return x;
}


struct bst* posearch(int n)
{
struct bst *x=root;
while(x->left!=NULL||x->right!=NULL)
{
	if(x->data>n)
	{
		if(x->left==NULL)
			return x;
		else
			x=x->left;
	}
	else if(x->data<n)
	{
		if(x->right==NULL)
			return x;
		else
			x=x->right;
	}
	else
		break;
}
return x;
}


void ins(int n)
{
struct bst *t=(struct bst*)malloc(sizeof(struct bst));
t->data=n;
t->left=NULL;
t->right=NULL;
if(root==NULL)
{
	root=t;
	return;
}
struct bst *x=posearch(n);
if(x->data>n)
	x->left=t;
else if(x->data<n)
	x->right=t;
else
	x->left=t;
}



void ino(struct bst *x)
{
if(x==NULL)
	return;
ino(x->left);
printf("%d",x->data);
ino(x->right);
}

void pre(struct bst *x)
{
if(x==NULL)
	return;
printf("%d",x->data);
pre(x->left);
pre(x->right);
}


void post(struct bst *x)
{
if(x==NULL)
	return;
post(x->left);
post(x->right);
printf("%d",x->data);
}


void main()
{
int c=0,t=0;
create();
printf("Enter choice:\n1 insert\n2 search\n3 In order Traversal\n4 post order\n5 pre order\n6 exit\n");
while(c!=6)
{
	scanf("%d",&c);
	switch(c)
	{
	case 1:printf("Enter the element: ");
	       scanf("%d",&t);
	       ins(t);
	       t=0;
	       break;
	case 2:printf("Enter the element: ");
    	   scanf("%d",&t);
    	   struct bst *x=search(t);
    	   if(x==NULL)
    		   printf("NOT FOUND");
    	   else
    	   {
    		   if(x->left!=NULL)
    		   printf("%d",x->left->data);
    	   if(x->right!=NULL)
    		   printf("%d",x->right->data);
    	   }
    	   t=0;
       		break;
	case 3:ino(root);
    	   break;
    case 4:post(root);
           break;
    case 5:pre(root);
           break;
	case 6:break;
	       
}
}
}
