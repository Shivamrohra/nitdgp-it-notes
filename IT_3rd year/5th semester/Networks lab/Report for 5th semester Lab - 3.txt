In addition to the physical addresses, the Internet requires an addressing convention to identify the individual devices. For this, IP addresses are used. They are unique and each address defines one and only one connection to the Internet.

If an application process running on one host wants to communicate with the application process running in another host, then it has to first establish connection to the other one with the help of IP address and then communicate with the particular application. For this, there is a port number for each application process.

An IP address is a 32-bit address that uniquely and universally defines the connection of a host to the Internet and port number is a 16 bit number allocated to a application process.

The network details can be found and manipulated using "ifconfig". Mapping of ports and running servers can be found using "netstat -ntlp"