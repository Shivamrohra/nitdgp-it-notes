#include<bits/stdc++.h>
using namespace std;
#define ll long long int

ll pw(ll a, ll b){
  ll r;
  if(b==0) return 1;
  r = pw(a,b/2);
  r = (r*r);
  if(b%2) r = (r*a);
  return r;
}

ll len(ll b)
{
    ll i=0,j;
    while(b!=0)
    {
        b/=10;
        i++;
    }
    return i;
}

void rabinkarp(char a[],ll b)
{
  ll l1,l;
  l1=len(b);
  l=strlen(a);

  if(l<l1)
  {
      printf("-1\n");
      return;
  }

  ll i,j,k=pw(10,l1-1),sum=0;

  for(i=0;i<l1;i++)
  {
      sum=sum*10+a[i]-'0';
  }

  while(i<l)
  {
      if(b==sum)
      {
          printf("%lld\n",i-l1);
          return ;
      }
      sum=(sum-(sum/k)*k)*10+a[i]-'0';
      i++;
  }
  printf("-1\n");
}

int main()
{
    char a[100000];
    ll b;
    scanf("%s %lld",a,&b);
    rabinkarp(a,b);
    return 0;
}
