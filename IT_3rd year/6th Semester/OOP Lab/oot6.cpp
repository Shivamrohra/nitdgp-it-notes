#include<iostream>
using namespace std;

class emp
{
    int emp_id;
    int age;
public:
    emp()
    {
        emp_id=-1;
        age=0;

    }
    emp(int e_id,int a)
    {
        emp_id=e_id;
        age=a;

    }
    void display()
    {

        cout<<"Employee Details :- "<<endl;
        cout<<"Employee ID : "<<emp_id<<endl;
        cout<<"Age : "<<age<<endl;

    }

};

class sal : public emp
{
    float salary;
public:
    sal() : emp()
    {
        salary = 0;

    }
    sal(int e_id,int a,float s) : emp(e_id,a)
    {
        salary =s;

    }
    void display()
    {
        emp::display();
        cout<<"Salary : "<<salary<<endl;

    }
};

class ret  : public sal
{
    int ret_age;
public:
    ret() : sal()
    {

        ret_age =0;
    }
    ret(int e_id,int a,float s,int r_age) : sal(e_id,a,s)
    {
        ret_age=r_age;
    }
    void display()
    {
        sal::display();
        cout<<"Retirement Age : "<<ret_age<<endl;
    }
};


int main()
{

    int a,ra,eid;
    float sal;
    cout<<"Enter employee details:- "<<endl;
    cout<<"ID: ";
    cin>>eid;
    cout<<"Age: ";
    cin>>a;
    cout<<"Salary: ";
    cin>>sal;
    cout<<"Retirement : ";
    cin>>ra;
    cout<<endl;
    ret x(eid,a,sal,ra);
    x.display();
    return 0;
}
